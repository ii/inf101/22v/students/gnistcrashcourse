package inheritance.composition;

public class Tyre {

    public final int friction;

    public Tyre(int friction) {
        this.friction = friction;
    }
}
