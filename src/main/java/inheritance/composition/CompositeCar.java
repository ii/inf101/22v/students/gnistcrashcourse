package inheritance.composition;

import abstraction.encapsulation.AbstractCar;
import abstraction.encapsulation.ICar;

import java.util.List;

public class CompositeCar extends AbstractCar {

    private final Engine engine;
    private final Tyre[] tyres;
    private final Body body;

    public CompositeCar(String licenseNumber, Engine engine, Tyre[] tyres, Body body) {
        super(licenseNumber);
        this.engine = engine;
        this.tyres = tyres;
        this.body = body;
    }

    @Override
    public void accelerate(int velocity) throws IllegalArgumentException {
        speed += velocity*engine.power;
        speed -= body.airResistance;
        for (Tyre tyre : tyres) {
            speed -= tyre.friction;
        }
    }

    @Override
    public void brake(int brakingForce) {
        // Advanced calculation
    }
}
