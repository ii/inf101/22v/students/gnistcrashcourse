package inheritance.composition;

public class Body {

    public final int airResistance;

    public Body(int airResistance) {
        this.airResistance = airResistance;
    }
}
