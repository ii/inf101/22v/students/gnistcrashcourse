package inheritance.composition;

public class Engine {

    public final int power;

    public Engine(int power) {
        this.power = power;
    }
}
