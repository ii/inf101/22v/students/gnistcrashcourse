package inheritance.composition;

import abstraction.encapsulation.AbstractCar;

public class AdvancedCar extends AbstractCar {

    private final int friction;

    public AdvancedCar(String licenseNumber, int friction) {
        super(licenseNumber);
        this.friction = friction;
    }

    @Override
    public void accelerate(int velocity) throws IllegalArgumentException {
        speed += (velocity - friction);
        System.out.println("Accelerated!");
    }

    @Override
    public void brake(int brakingForce) {
        // Advanced calculation
    }
}
