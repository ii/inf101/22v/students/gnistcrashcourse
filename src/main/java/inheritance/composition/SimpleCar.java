package inheritance.composition;

import abstraction.encapsulation.AbstractCar;

public class SimpleCar extends AbstractCar {

    public SimpleCar(String licenseNumber) {
        super(licenseNumber);
    }

    @Override
    public void accelerate(int velocity) throws IllegalArgumentException {
        speed += velocity;
        System.out.println("Accelerated!");
    }

    @Override
    public void brake(int brakingForce) {
        speed -= brakingForce;
    }
}
