package v21.q5;

public class Counter implements ICounter {

    private int count;

    public Counter() {
        count = 0;
    }

    @Override
    public int read() {
        return count;
    }

    @Override
    public void increase() {
        count++;
    }

    @Override
    public void reset() {
        count = 0;
    }
}
