package v21.q5;

/**
 * datainvariant: the count can never be negative.
 */
public interface ICounter {

    /**
     * reads the count
     * @return the count
     */
    int read();

    /**
     * increases the count by 1
     */
    void increase();

    /**
     * resets the count 0
     */
    void reset();
}
