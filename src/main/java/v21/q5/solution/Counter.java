package v21.q5.solution;

public class Counter implements ICounter {

    private int count;

    public Counter() {
        count = 0;
    }

    @Override
    public int read() {
        return count;
    }

    @Override
    public void increment() {
        count++;
        if (count < 0) {
            throw new IllegalStateException("Integer overflow, counter became negative");
        }
    }

    @Override
    public void reset() {
        count = 0;
    }
}
