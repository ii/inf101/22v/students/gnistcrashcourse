package v21.q5.solution;

public interface ICounter {

    /**
     * Read the count.
     *
     * @return the count
     * */
    int read();

    /**
     * Increases the count by 1.
     * */
    void increment();

    /**
     * Resets the count to 0.
     * */
    void reset();
}
