### 1. Typeparameter

- Alle datatyper som er subtyper av *ShoppingItem*

### 2. Prinsipper i Objektorientert Programmering

- Abstraction
- Encapsulation

### 3. Inheritance

- B
- C
- E
- F
- H
- I

![img.png](../../../resources/img.png)
Credit: Martin Vatshelle

### 4. Typesignaturen til Collections.sort

- "static" betyr at metoden kan kalles uten å først konstruere et Collections-objekt.
- Dersom T er sammenlignbar med seg selv, så kan List sorteres.
- Dersom T utvider en klasse som er sammenlignbar med seg selv, så kan List sorteres.