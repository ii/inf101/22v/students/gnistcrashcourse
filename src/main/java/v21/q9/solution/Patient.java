package v21.q9.solution;

public class Patient implements Comparable<Patient> {

    private String name;
    private UnderlyingConditionGrade underlyingConditionGrade;
    private int age;

    public Patient(String name, int age, UnderlyingConditionGrade underlyingCondition) {
        this.name = name;
        this.age = age;
        this.underlyingConditionGrade = underlyingCondition;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Patient o) {
        if (this.underlyingConditionGrade != o.underlyingConditionGrade) {
            return Integer.compare(o.underlyingConditionGrade.getValue(), this.underlyingConditionGrade.getValue());
        } else {
            return Integer.compare(o.age, this.age);
        }
    }
}
