package v21.q6.solution;

import java.util.Objects;

/**
 * A shopping item is an item you purchase at the store. A shopping item has
 an
 * item type, such as chicken, fruit, vegetables, cleaning tools, soda, snacks
 ,
 * etc.,
 * and a brand, such as Fjordland, Tine, Prior, etc.
 *
 * @author Sondre Bolland
 *
 */
public class ShoppingItem {

    /**
     * Type of shopping item
     */
    private String itemType;
    /**
     * Brand (producer) of shopping item
     */
    private String brand;

    public ShoppingItem(String itemType, String brand) {
        this.itemType = itemType;
        this.brand = brand;
    }

    public String getItemType() {
        return itemType;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingItem that = (ShoppingItem) o;
        return Objects.equals(this.itemType, that.itemType) && Objects.equals(this.brand, that.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemType, brand);
    }
}