package v21.q8;

import v21.q8.solution.FridgeItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Fridge implements IFridge {

    int maxItems;
    List<FridgeItem> items;

    public Fridge(int maxItems) {
        this.maxItems = maxItems;
        items = new ArrayList<>(maxItems);
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return maxItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.remove(item)) {
            throw new IllegalArgumentException("Item not in Fridge");
        }
    }

    @Override
    public FridgeItem takeOut(String itemName) {
        List<FridgeItem> foundItems = new ArrayList<>();
        for (FridgeItem item : items) {
            if (item.getName().equals(itemName)) {
                foundItems.add(item);
            }
        }
        if (foundItems.isEmpty()) {
            throw new IllegalArgumentException("Item not in Fridge");
        }
        FridgeItem foundItem = Collections.min(foundItems);
        items.remove(foundItem);
        return foundItem;
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = getExpiredItems();
        items.removeAll(expiredItems);
        return expiredItems;
    }

    private List<FridgeItem> getExpiredItems() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        return expiredItems;
    }
}
