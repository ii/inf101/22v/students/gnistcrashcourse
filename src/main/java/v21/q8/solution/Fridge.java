package v21.q8.solution;

import v21.q8.IFridge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Fridge implements IFridge {

    private final List<FridgeItem> items;
    private final int size;

    public Fridge(int size) {
        this.size = size;
        items = new ArrayList<>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() >= totalSize()) {
            return false;
        }
        return items.add(item);
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)) {
            throw new IllegalArgumentException("Item is not in fridge");
        }
        items.remove(item);
    }

    @Override
    public FridgeItem takeOut(String itemName) {
        List<FridgeItem> foundItems = new ArrayList<>();
        for (FridgeItem item : items) {
            if (item.getName().equals(itemName)) {
                foundItems.add(item);
            }
        }
        if (foundItems.isEmpty()) {
            throw new IllegalArgumentException("Item is not in fridge");
        }
        FridgeItem toTakeOut = Collections.min(foundItems);
        takeOut(toTakeOut);
        return toTakeOut;
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = getExpiredItems();
        items.removeAll(expiredItems);
        return expiredItems;
    }

    /**
     * @return List of expired items
     */
    private List<FridgeItem> getExpiredItems() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        return expiredItems;
    }
}
