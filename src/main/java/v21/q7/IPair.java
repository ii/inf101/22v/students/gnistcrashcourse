package v21.q7;

public interface IPair<F, S> {

    /**
     * @return the first element in IPair
     */
    F getFirst();

    /**
     * @return the second element in IPair
     */
    S getSecond();
}
