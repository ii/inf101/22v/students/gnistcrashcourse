package v21.q7;

public class IntegerPair implements IIntegerPair {

    private final int first;
    private final int second;

    public IntegerPair(int first, int second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public int add() {
        return getFirst() + getSecond();
    }

    @Override
    public Integer getFirst() {
        return first;
    }

    @Override
    public Integer getSecond() {
        return second;
    }
}
