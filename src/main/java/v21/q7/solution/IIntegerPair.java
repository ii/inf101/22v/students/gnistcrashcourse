package v21.q7.solution;

public interface IIntegerPair extends IPair<Integer, Integer> {

    /**
     * Sums the first and second Integer element in the IIntegerPair
     *
     * @return the sum
     */
    int sum();
}
