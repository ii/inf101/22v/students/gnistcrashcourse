package v21.q7.solution;

public interface IPair<F, S> {

    /**
     * @return the first IPair element
     */
    F getFirst();

    /**
     * @return the second IPair element
     */
    S getSecond();
}
