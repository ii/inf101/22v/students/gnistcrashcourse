package v21.q7.solution;

public class IntegerPair implements IIntegerPair {

    private final Integer first;
    private final Integer second;

    public IntegerPair(Integer first, Integer second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public int sum() {
        return getFirst() + getSecond();
    }

    @Override
    public Integer getFirst() {
        return first;
    }

    @Override
    public Integer getSecond() {
        return second;
    }
}
