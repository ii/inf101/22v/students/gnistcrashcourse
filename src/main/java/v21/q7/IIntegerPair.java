package v21.q7;

public interface IIntegerPair extends IPair<Integer, Integer> {

    /**
     * @return the sum of first and second element in IIntegerPair
     */
    int add();
}
