package abstraction.encapsulation;

public interface ICar {

    /**
     * @return License number of the Car
     */
    String getLicenseNumber();

    /**
     * @return The speed of the Car
     */
    int getSpeed();

    /**
     * Accelerates the Car by applying velocity
     * @param velocity speed to accelerate
     * @throws IllegalArgumentException when velocity is negative
     */
    void accelerate(int velocity) throws IllegalArgumentException;

    /**
     * Slows the Car down by applying braking force
     * @param brakingForce force to slow down with
     */
    void brake(int brakingForce);
}
