package abstraction.encapsulation;

import java.util.Objects;

public abstract class AbstractCar implements ICar {

    private final String licenseNumber;
    protected int speed;

    public AbstractCar(String licenseNumber) {
        this.licenseNumber = licenseNumber;
        speed = 0;
    }

    @Override
    public String getLicenseNumber() {
        return licenseNumber;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractCar that = (AbstractCar) o;
        return this.licenseNumber.equals(that.licenseNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(licenseNumber);
    }
}
