package abstraction.encapsulation;

public class Car implements ICar {

    private final String licenseNumber;
    private int speed = 0;

    public Car(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    private void checkVelocity(int velocity) {
        if (velocity < 0) {
            throw new IllegalArgumentException("Velocity cannot be negative!");
        }
    }

    @Override
    public void accelerate(int velocity) throws IllegalArgumentException {
        checkVelocity(velocity);

        speed += velocity;
        System.out.println("Accelerated!");
    }

    @Override
    public String getLicenseNumber() {
        return licenseNumber;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public void brake(int brakingForce) {
        speed -= brakingForce;
    }
}

//    public static void main(String[] args) {
//        Car c = new Car("A", 2);
//        String s = c.getLicenseNumber();
//        c.accelerate(0);
//        c.brake(0);
//    }