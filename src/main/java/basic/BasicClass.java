package basic;

public class BasicClass {

    String myString;
    int myInt;

    public BasicClass(String myString, int myInt) {
        this.myString = myString;
        this.myInt = myInt;
    }

    public static int myMethod(int firstInt) {
        int secondInt = 2;
        if (firstInt >= 3) {
            int thirdInt = secondInt;
            secondInt++;
            thirdInt--;
        }
        System.out.println(secondInt);
//        System.out.println(thirdInt);

        return firstInt + secondInt;
    }

    public static void main(String[] args) {
//        int firstInt = myMethod(2);
//        System.out.println(firstInt);
    }
}
