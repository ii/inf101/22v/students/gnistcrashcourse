import abstraction.encapsulation.Car;
import abstraction.encapsulation.ICar;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class CarTest {

    ICar car;

    @BeforeEach
    void setup() {
        car = new Car("AB12345");
    }

    @Test
    void testLicenseNumber() {
        assertEquals("AB12345", car.getLicenseNumber());
    }

    @Test
    void testCarDoesAccelerate() {
        assertEquals(0, car.getSpeed());
        car.accelerate(2);
        assertEquals(2, car.getSpeed());
    }

    @Test
    void testCarThrowsOnNegativeVelocity() {
        try {
            car.accelerate(-2);
            fail();
        } catch (IllegalArgumentException ignored) {}
    }
}
